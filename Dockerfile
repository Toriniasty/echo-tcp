FROM golang:latest as builder
WORKDIR /go/src/app
COPY . .
RUN CGO_ENABLED=0 go build -o echoserver-tcp .

FROM alpine:latest
COPY --from=builder /go/src/app/echoserver-tcp /usr/bin/echoserver-tcp

ENTRYPOINT ["/usr/bin/echoserver-tcp"]
