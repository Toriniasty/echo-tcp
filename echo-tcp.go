package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	flag "github.com/spf13/pflag"
)

var listen string
var port string
var hostname string
var err error

func init() {
	flag.StringVarP(&listen, "listen", "l", "0.0.0.0", "Listen address. Default: 0.0.0.0")
	flag.StringVarP(&port, "port", "p", "9300", "Listen port. Default: 9300")
	flag.Parse()

	hostname, err = os.Hostname()
	if err != nil {
		panic(err)
	}
}

func main() {
	// Listen for incoming connections.
	l, err := net.Listen("tcp", listen+":"+port)
	if err != nil {
		log.Println("Error listening:", err.Error())
		os.Exit(1)
	}

	// Close the listener when the application closes.
	defer l.Close()
	log.Printf("[TCP] Listening on %s:%s. Hostname: %s", listen, port, hostname)

	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			log.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}

		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	// Make a buffer to hold incoming data.
	buf := make([]byte, 1024)

	// Read the incoming connection into the buffer.
	rlen, err := conn.Read(buf)
	if err != nil {
		log.Println("Error reading:", err.Error())
	}
	// Read the incoming connection into the buffer.
	message := strings.TrimRight(string(buf[:rlen]), "\n")

	// Send a response back to person contacting us.
	logStringClient := fmt.Sprintf("[TCP] Incoming connection from: %s, My hostname: %s, Message: %s\n", conn.RemoteAddr(), hostname, message)
	logStringServer := fmt.Sprintf("[TCP] Incoming connection from: %s, Message: %s\n", conn.RemoteAddr(), message)
	conn.Write([]byte(logStringClient))
	log.Print(logStringServer)

	// Close the connection when you're done with it.
	conn.Close()
}
