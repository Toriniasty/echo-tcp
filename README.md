# Simple TCP echo server

## Parameters

```bash
  -l, --listen string   Listen address. Default: 0.0.0.0 (default "0.0.0.0")
  -p, --port int        Listen port. Default: 9300 (default 9300)
```

## How to use

### Start the server

By either:

1) Compiling it and running

```bash
$ go build
$ ./echo-tcp
2021/07/12 18:08:53 [TCP] Listening on 0.0.0.0:9300. Hostname: laptopix
```

2) Just running the code

```bash
$ go run echo-tcp.go 
2021/07/12 18:08:53 [TCP] Listening on 0.0.0.0:9300. Hostname: laptopix
```

### Connect

```bash
$ echo zzz | nc localhost 9300
[TCP] Incoming connection from: ::1:42175, My hostname: laptopix, Message: zzz
```
